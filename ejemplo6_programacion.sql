﻿DROP DATABASE IF EXISTS ej_programacion6;
CREATE DATABASE IF NOT EXISTS ej_programacion6;
USE ej_programacion6;

/** 1 - Crear un disparador para la tabla ventas 
        para que cuando metas un registro nuevo 
        te calcule el total automáticamente.
        El total debe ser unidades por precio.
        Recordar que para el insert 
        solo podéis utilizar NEW. 
        Además, debéis recordar que para actualizar 
        los valores de una tabla en la inserción 
        debe ser BEFORE (con el AFTER podéis actualizar 
        otra tabla, pero no la misma).
**/
DELIMITER //
  CREATE OR REPLACE TRIGGER ventasBI
    BEFORE INSERT 
    ON ventas  
                
    FOR EACH ROW
 
    BEGIN
     SET NEW.total = NEW.unidades * NEW.precio;  
    END //
DELIMITER ;

INSERT INTO ventas (producto, precio, unidades)
    VALUES ('p2', 15, 20);

  SELECT * FROM ventas v;

/**
2.- Crear un disparador para la tabla ventas 
    para que cuando inserte un registro 
    me sume el total a la tabla productos 
    (en el campo cantidad).
**/

DELIMITER //
  CREATE OR REPLACE TRIGGER ventasAI
    AFTER INSERT
    ON ventas
    FOR EACH ROW
    BEGIN
      UPDATE productos p 
        SET p.cantidad = p.cantidad + NEW.total 
        WHERE p.producto = NEW.producto;
    END //
  DELIMITER ;

  
  INSERT INTO ventas (producto, precio, unidades)
    VALUES ('p5', 50, 10);

  SELECT * FROM ventas v;
  SELECT * FROM productos p;

/**
3.- Crear un disparador para la tabla ventas 
    para que cuando actualices un registro nuevo
    te calcule el total automáticamente.

   -El total debe ser unidades por precio.
**/

DELIMITER //
  CREATE OR REPLACE TRIGGER ventasBU
    BEFORE UPDATE
    ON ventas
    FOR EACH ROW
    BEGIN
      SET NEW.total = NEW.unidades * NEW.precio;
    END //
  DELIMITER ;

  
  UPDATE ventas v 
    SET v.unidades = 200
    WHERE v.id = 4;

  SELECT * FROM ventas v;

/**
4.- Crear un disparador para la tabla ventas 
    para que cuando actualice un registro me
    sume el total a la tabla productos 
    (en el campo cantidad).
**/

DELIMITER //
  CREATE OR REPLACE TRIGGER ventasAU
    AFTER UPDATE
    ON ventas
    FOR EACH ROW
    BEGIN
      UPDATE productos p 
        SET p.cantidad = p.cantidad + (NEW.total - OLD.total) 
        WHERE p.producto = NEW.producto;
    END //
  DELIMITER ;

  
  UPDATE ventas v 
    SET v.unidades = 60
    WHERE v.id = 4;

  SELECT * FROM ventas v;
  SELECT * FROM productos p;

/**
5.- Crear un disparador para la tabla productos 
    que si cambia el código del producto te
    sume todos los totales de ese producto 
    de la tabla ventas
**/

  DELIMITER //
  CREATE OR REPLACE TRIGGER productosBU
    BEFORE UPDATE
    ON productos
    FOR EACH ROW
    BEGIN
      SET NEW.cantidad = (
        SELECT SUM(v.total) FROM ventas v
          WHERE v.producto = NEW.producto);
    END //
  DELIMITER ;
  
 
  UPDATE productos p 
    SET p.producto = 'p6'
    WHERE p.producto = 'p2';

  SELECT * FROM productos p;

/**
6.- Crear un disparador para la tabla productos 
    que si eliminas un producto te elimine
    todos los productos del mismo código 
    en la tabla ventas
**/

  DELIMITER //
  CREATE OR REPLACE TRIGGER productosAD
    AFTER DELETE
    ON productos
    FOR EACH ROW
    BEGIN
      DELETE FROM ventas
        WHERE OLD.producto = producto;
    END //
  DELIMITER ;

  
  DELETE FROM productos 
    WHERE producto = 'p3';

  SELECT * FROM ventas v;
  SELECT * FROM productos p;

/**
7.- Crear un disparador para la tabla ventas 
    que si eliminas un registro te reste el total 
    del campo cantidad de la tabla productos 
    (en el campo cantidad).
**/
DELIMITER //
  CREATE OR REPLACE TRIGGER ventasAD
    AFTER DELETE
    ON ventas
    FOR EACH ROW
    BEGIN
      UPDATE productos  
        SET cantidad = cantidad - OLD.total 
        WHERE producto = OLD.producto;
    END //
  DELIMITER ;

  
  DELETE FROM ventas 
    WHERE id = 3;

  DELETE FROM ventas 
    WHERE id = 20;

  SELECT * FROM ventas v;
  SELECT * FROM productos p;

/**
8.- Modificar el disparador 3 para que modifique la tabla productos 
    actualizando el valor del campo cantidad 
    en funcion del total.
**/
DELIMITER //
  CREATE OR REPLACE TRIGGER ventasBU
    BEFORE UPDATE
    ON ventas
    FOR EACH ROW
    BEGIN
      SET NEW.total = NEW.unidades * NEW.precio;
      UPDATE productos p 
     SET p.cantidad=p.cantidad+(new.total-old.total)   
    WHERE p.producto=new.producto;
     
    END //
  DELIMITER ;

UPDATE ventas v
  SET v.unidades=125
  WHERE v.id=8;

SELECT * FROM ventas v;
SELECT * FROM productos p;